import numpy as np
import cv2

cap = cv2.VideoCapture(0)
w,h = 640,480
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
#out = cv2.VideoWriter('output.mp4',fourcc, 20.0, (w,h))
fgbg = cv2.createBackgroundSubtractorMOG2()

prev_frame=[]
while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:

        if len(prev_frame) > 0: # is not None:
            fgmask = fgbg.apply(frame) # fore ground maks on current frame                
            res = cv2.bitwise_and(frame,prev_frame[0], mask=fgmask) # bitwise between current, 1st frame of array, with mask
            cv2.imshow('frame',res)
            if len(prev_frame) > 10:
                prev_frame = []
        prev_frame.append(frame)    
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished
cap.release()
#out.release()
cv2.destroyAllWindows()