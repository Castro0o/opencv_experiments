import cv2
import numpy as np

cap = cv2.VideoCapture(0)

fgbg = cv2.createBackgroundSubtractorMOG2()

while(1):

    # Take each frame
    _, frame = cap.read()
    fgmask = fgbg.apply(frame)


    dst = cv2.bitwise_and(frame,frame, mask=fgmask)

    cv2.imshow('mask',fgmask)
    cv2.imshow('frame',dst)

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cv2.destroyAllWindows()
