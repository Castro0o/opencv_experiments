import cv2
import numpy as np

cap = cv2.VideoCapture(0)

i = 0
while(1):

    # Take each frame
    _, frame = cap.read()

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([0,50,255])
    upper_blue = np.array([130,255,255])

# define range of blue color in HSV
    lower_red = np.array([0,50,50])
    upper_red = np.array([255,255,130])


    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    mask_red = cv2.inRange(hsv, lower_red, upper_red)

    frame_flip = cv2.flip(frame,0)
    # Bitwise-AND mask and original image
    # and flipped image
    res = cv2.bitwise_and(frame,frame, mask=mask)
    res_red = cv2.bitwise_and(frame,frame, mask=mask_red)
    res_flip_x = cv2.flip(res_red,1) # flips image on x axis
    dst1 = cv2.addWeighted(res,0.5, res_flip_x , 0.5, 0) # superimposes

    i += 1
    
    #cv2.imshow('frame',frame)
    cv2.imshow('mask',mask_red)
    cv2.imshow('res',dst1)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
