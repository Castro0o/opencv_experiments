import numpy as numpy
import cv2

cap = cv2.VideoCapture(0)
w,h = 640,480
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter('output.mp4',fourcc, 20.0, (w,h))

i=0
while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
            for pixel in frame:
                #if type(pixel) !=  'numpy.ndarray':
                print(pixel.dtype) 
            inverted_rgb = [pixel*2 for pixel in frame]
            #print(frame, frame)    
            #if i%10 == 0: # only allow even frames to be write and displayed
            out.write(inverted_rgb)
            i += 1

            cv2.imshow('frame',inverted_rgb)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    else:
        break

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()