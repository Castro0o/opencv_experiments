import cv2
import numpy as np

cap = cv2.VideoCapture(0)


i = 0
while(1):

    # Take each frame
    _, frame = cap.read()
    kernel = np.ones((25,25),np.uint8)
    dilate = cv2.dilate(frame,kernel,iterations = 1)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(dilate, cv2.COLOR_BGR2HSV)
    # define range of blue color in HSV
    lower_blue = np.array([0,50,255])
    upper_blue = np.array([130,255,255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)

    dst = cv2.addWeighted(dilate,0.5, frame , 0.5, 0) # superimposes
    res = cv2.bitwise_and(frame,dilate, mask=mask)


    cv2.imshow('mask',mask)
    cv2.imshow('dilate',dilate)
    cv2.imshow('bitwise',res)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cv2.destroyAllWindows()
