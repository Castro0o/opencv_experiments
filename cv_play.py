import numpy as np
import cv2
fourcc = cv2.VideoWriter_fourcc(*'X264')
cap = cv2.VideoCapture('Roma.mkv')

w,h = 640,480
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'X264')
out = cv2.VideoWriter('output1.avi',fourcc, 20.0, (w,h))

while(cap.isOpened()):
    ret, frame = cap.read()

    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    out.write(frame)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
