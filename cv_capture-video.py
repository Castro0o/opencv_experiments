import numpy as numpy
import cv2

cap = cv2.VideoCapture(0)
w,h = 640,480
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter('output.mp4',fourcc, 20.0, (w,h))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:

        b,g,r = cv2.split(frame)
        #merged = 

        frame_flip = cv2.flip(frame,0)
        frame_flip_x = cv2.flip(frame,1)

        # center = (w / 2, h / 2) 
        # M = cv2.getRotationMatrix2D(center, 80, 2) 
        # frame_rotate = frame
        # rotated90 = cv2.warpAffine(frame, M, (h, w)) 
        dst = cv2.bitwise_or(frame_flip,frame_flip_x)
        dst1 = cv2.addWeighted(dst,0.5, frame , 0.5, 0)


        #shuffle = cv2.randShuffle(frame,iterFactor=0.001)
        #rotate = cv2.rotate(frame,rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE )
        # write the flipped frame
        out.write(dst1)

        cv2.imshow('frame',dst)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()